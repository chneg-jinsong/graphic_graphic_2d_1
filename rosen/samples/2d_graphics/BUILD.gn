# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/arkui/ace_engine/ace_config.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")

ohos_executable("drawing_engine_sample") {
  install_enable = true
  cflags = [
    "-Wall",
    "-Werror",
    "-Wno-unused-parameter",
    "-Wno-missing-field-initializers",
    "-Wno-unused-variable",
    "-Werror,-Wreturn-type",
  ]

  sources = [
    "drawing_engine_sample.cpp",
    "main.cpp",
  ]

  sources += [
    "drawing_engine/canvas_context.cpp",
    "drawing_engine/drawing_proxy.cpp",
    "drawing_engine/drawing_surface/surface_frame_ohos_raster.cpp",
    "drawing_engine/drawing_surface/surface_ohos.cpp",
    "drawing_engine/drawing_surface/surface_ohos_raster.cpp",
    "drawing_engine/software_render_backend.cpp",
  ]

  if (ace_enable_gpu) {
    if (graphic_2d_feature_enable_vulkan) {
      sources += [
        "drawing_engine/drawing_surface/surface_frame_ohos_vulkan.cpp",
        "drawing_engine/drawing_surface/surface_ohos_vulkan.cpp",
        "drawing_engine/vulkan_render_backend.cpp",
      ]
    } else {
      sources += [
        "drawing_engine/drawing_surface/surface_frame_ohos_gl.cpp",
        "drawing_engine/drawing_surface/surface_ohos_gl.cpp",
        "drawing_engine/egl_manager.cpp",
        "drawing_engine/gles_render_backend.cpp",
      ]
    }
  }

  configs = []

  include_dirs = [
    "include",
    "drawing_engine",
    "drawing_engine/drawing_surface",
    "$graphic_2d_root/rosen/modules/composer/hdi_backend/include",
    "$graphic_2d_root/utils/sync_fence/export",
    "$graphic_2d_root/rosen/include/common",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
  ]

  sources += [
    "benchmarks/benchmark_config.cpp",
    "benchmarks/benchmark_dcl/drawing_command.cpp",
    "benchmarks/benchmark_dcl/drawing_playback.cpp",
    "benchmarks/benchmark_dcl/skia_recording.cpp",
    "benchmarks/benchmark_multithread/drawing_mutilthread.cpp",
    "benchmarks/benchmark_singlethread/drawing_singlethread.cpp",
  ]

  include_dirs += [
    "benchmarks",
    "benchmarks/benchmark_singlethread",
    "benchmarks/benchmark_multithread",
    "benchmarks/benchmark_api",
    "benchmarks/benchmark_dcl",
  ]

  deps = [
    "$graphic_2d_root:libsurface",
    "$graphic_2d_root/rosen/modules/composer:libcomposer",
    "$graphic_2d_root/utils:sync_fence",
  ]

  if (defined(use_new_skia) && use_new_skia) {
    deps += [ "//third_party/skia:skia_ohos" ]
  } else {
    deps += [ "//third_party/flutter/build/skia:ace_skia_ohos" ]
  }

  public_deps = libgl
  defines = gpu_defines

  #defines = [ "USE_CANVASKIT0310_SKIA" ]
  #defines += gpu_defines
  deps += [
    #"//third_party/skia:skia",
  ]
  if (graphic_2d_feature_enable_vulkan) {
    include_dirs += [ "//third_party/flutter/engine/flutter/vulkan" ]
  }
  public_deps += [
    "$graphic_2d_root:libvulkan",
    "$graphic_2d_root/rosen/modules/render_service_base:librender_service_base",
    "//third_party/openssl:libcrypto_shared",
  ]
  external_deps = [
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "image_framework:image_native",
    "init:libbegetutil",
    "ipc:ipc_core",
  ]

  subsystem_name = "graphic"
  part_name = "graphic_2d"
}

ohos_executable("drawing_sample_rs") {
  install_enable = true

  sources = [ "drawing_c_sample.cpp" ]

  include_dirs = [
    "$graphic_2d_root/rosen/modules/2d_graphics/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/src",
    "$graphic_2d_root/rosen/modules/render_service_base/src",
    "$graphic_2d_root/rosen/modules/render_service_base/include",
    "$graphic_2d_root/rosen/modules/render_service_client",
    "$graphic_2d_root/rosen/include/common",
    "//foundation/window/window_manager/interfaces/innerkits/wm",
  ]

  deps = [
    "$graphic_2d_root/rosen/modules/2d_graphics:2d_graphics",
    "$graphic_2d_root/rosen/modules/render_service_base:librender_service_base",
    "$graphic_2d_root/rosen/modules/render_service_client:librender_service_client",
    "//third_party/zlib:libz",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

ohos_executable("drawing_sample_canvaskit0310") {
  install_enable = true

  sources = [ "drawing_sample_canvaskit0310.cpp" ]

  include_dirs = [
    "$graphic_2d_root/rosen/modules/composer/hdi_backend/include",
    "$graphic_2d_root/rosen/include/common",
    "$graphic_2d_root/interfaces/inner_api/common",
    "$graphic_2d_root/rosen/modules/composer/vsync/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/src",
  ]

  deps = [
    "$graphic_2d_root:libsurface",
    "$graphic_2d_root/rosen/modules/2d_graphics:2d_graphics_canvaskit0310",
    "$graphic_2d_root/rosen/modules/composer:libcomposer",
    "$graphic_2d_root/utils:libgraphic_utils",
    "$graphic_2d_root/utils:sync_fence",
  ]

  external_deps = [
    "hilog:libhilog",
    "ipc:ipc_core",
  ]

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}
