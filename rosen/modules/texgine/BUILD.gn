# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")

is_ok = true

## Build libtexgine.so {{{
config("libtexgine_config") {
  visibility = [ ":libtexgine" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
  ]

  if (enable_text_gine) {
    defines = [ "USE_GRAPHIC_TEXT_GINE" ]
  } else {
    defines = [
      "LOGGER_ENABLE_SCOPE",
      "TEXGINE_ENABLE_DEBUGLOG",
    ]
  }

  include_dirs = [
    "src",
    "export",
  ]
}

config("libtexgine_public_config") {
  include_dirs = [
    "export",
    "texgine_drawing",
    "//third_party/skia/third_party/externals/harfbuzz/src",
  ]
}

ohos_shared_library("libtexgine") {
  if (is_ok) {
    sources = [
      "src/bidi_processer.cpp",
      "src/char_groups.cpp",
      "src/dynamic_file_font_provider.cpp",
      "src/dynamic_font_provider.cpp",
      "src/dynamic_font_style_set.cpp",
      "src/font_collection.cpp",
      "src/font_config.cpp",
      "src/font_manager.cpp",
      "src/font_parser.cpp",
      "src/font_providers.cpp",
      "src/font_styles.cpp",
      "src/init.cpp",
      "src/line_breaker.cpp",
      "src/measurer.cpp",
      "src/measurer_impl.cpp",
      "src/mock.cpp",
      "src/opentype_parser/cmap_parser.cpp",
      "src/opentype_parser/cmap_table_parser.cpp",
      "src/opentype_parser/name_table_parser.cpp",
      "src/opentype_parser/opentype_basic_type.cpp",
      "src/opentype_parser/post_table_parser.cpp",
      "src/opentype_parser/ranges.cpp",
      "src/shaper.cpp",
      "src/system_font_provider.cpp",
      "src/texgine_exception.cpp",
      "src/text_breaker.cpp",
      "src/text_converter.cpp",
      "src/text_merger.cpp",
      "src/text_reverser.cpp",
      "src/text_shaper.cpp",
      "src/text_span.cpp",
      "src/text_style.cpp",
      "src/typeface.cpp",
      "src/typography_builder_impl.cpp",
      "src/typography_impl.cpp",
      "src/typography_style.cpp",
      "src/typography_types.cpp",
      "src/utils/exlog.cpp",
      "src/utils/logger.cpp",
      "src/utils/memory_reporter.cpp",
      "src/utils/trace_ohos.cpp",
      "src/variant_font_style_set.cpp",
      "src/variant_span.cpp",
      "src/word_breaker.cpp",
    ]

    configs = [
      ":libtexgine_config",
      "//build/config/compiler:exceptions",
    ]

    public_configs = [ ":libtexgine_public_config" ]

    platform = current_os
    if (platform == "mingw") {
      platform = "windows"
    }

    public_deps = [
      "texgine_drawing:libtexgine_drawing",
      "//third_party/bounds_checking_function:libsec_static",
      "//third_party/jsoncpp:jsoncpp",
    ]

    if (enable_text_gine) {
      cflags_cc = [ "-Wno-c++17-extensions" ]
      cflags = [ "-Wno-c++17-extensions" ]
      if (platform == "ohos") {
        defines = [
          "BUILD_NON_SDK_VER",
          "LOGGER_ENABLE_SCOPE",
          "TEXGINE_ENABLE_DEBUGLOG",
        ]

        external_deps = [
          "c_utils:utils",
          "hilog:libhilog",
          "hitrace:hitrace_meter",
        ]

        public_deps +=
            [ "//third_party/flutter/build/icu:ace_libicu_$platform" ]
      } else {
        sources -= [ "src/utils/trace_ohos.cpp" ]
        public_deps += [ "//third_party/skia/third_party/icu" ]
      }
    } else {
      public_deps +=
          [ "$graphic_2d_root/rosen/build/flutter/icu:rosen_libicu_$platform" ]

      external_deps = [
        "c_utils:utils",
        "hilog:libhilog",
        "hitrace:hitrace_meter",
      ]
    }
  }

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}
## Build libtexgine.so }}}
